from python_imagesearch.imagesearch import imagesearch
import pyautogui
import time

ICONES_PLANTACOES = ["./img/icone_plantacao_milho.png", "./img/icone_plantacao_nabo.png", "./img/icone_plantacao_repolho.png", "./img/icone_plantacao_silvestre.png"]
ICONES_MOINHO = ['./img/icone_celeiro_milho.png', './img/icone_celeiro_nabo.png', './img/icone_celeiro_suinos.png']
POSICAO_BOTAO_PLANTAR_MENU = []
PRIMEIRA_ABERTURA_MENU_PLANTAR = True
# 0-silvestres 1-Milho 2-Nabo 3-Repolho 
ITEM_PLANTAR = [1, 1, 2, 1, 2, 1, 3]
ITEM_CONTADOR = 0

POSICAO_BOTAO_MOINHO = []
PRIMEIRA_ABERTURA_MENU_MOINHO = True
ITEM_MOINHO = [0, 1]
ITEM_CONTADOR_MOINHO= 0

def posicaoBotoes():
  pos = imagesearch("./img/icone_plantar_menu.png")
  if pos[0] != -1:
    print("Pegando Posicao botoes")
    POSICAO_BOTAO_PLANTAR_MENU.append((pos[0]+260, pos[1]+170))
    POSICAO_BOTAO_PLANTAR_MENU.append((pos[0]+590, pos[1]+170))
    POSICAO_BOTAO_PLANTAR_MENU.append((pos[0]+260, pos[1]+330))
    POSICAO_BOTAO_PLANTAR_MENU.append((pos[0]+590, pos[1]+330))
    PRIMEIRA_ABERTURA_MENU_PLANTAR = False

def posicaoBotoesMoinho():
  pos = imagesearch("./img/icone_celeiro_menu.png")
  if pos[0] != -1:
    print("Pegando posicao botoes Silo")
    POSICAO_BOTAO_MOINHO.append((pos[0]+75, pos[1]+100))
    POSICAO_BOTAO_MOINHO.append((pos[0]+395, pos[1]+100))
    POSICAO_BOTAO_MOINHO.append((pos[0]+75, pos[1]+264))
    POSICAO_BOTAO_MOINHO.append((pos[0]+395, pos[1]+264))
    PRIMEIRA_ABERTURA_MENU_MOINHO = False

def colherPlantar():
  time.sleep(0.3)
  for plantacoes in ICONES_PLANTACOES:
    pos = imagesearch(plantacoes)
    if pos[0] != -1:
      break
  if pos[0] != -1:
    pyautogui.click(pos[0]+10, pos[1]+10)
    print("Colheita feita")
    time.sleep(2)
    vender()
    pyautogui.click(pos[0]+20, pos[1]+20)
    print("Abrindo menu")
    time.sleep(2)
    plantar()

def plantar():
  pos = imagesearch("./img/icone_plantar.png")
  if pos[0] != -1:
    pyautogui.click(pos)
    print('Plantando')
    time.sleep(1)
    if(PRIMEIRA_ABERTURA_MENU_PLANTAR):
      posicaoBotoes()
    pos = imagesearch("./img/icone_plantar_menu.png")
    if pos[0] != -1:
      global ITEM_CONTADOR
      pyautogui.click(POSICAO_BOTAO_PLANTAR_MENU[ITEM_PLANTAR[ITEM_CONTADOR]])
      if ITEM_CONTADOR == len(ITEM_PLANTAR) -1:
        ITEM_CONTADOR = 0;
      else:
        ITEM_CONTADOR += 1
    return True
  return False

def moinho():
  time.sleep(0.3)
  for icone in ICONES_MOINHO:
    pos = imagesearch(icone)
    if pos[0] != -1:
      break
  if pos[0] != -1:
    pyautogui.click(pos[0]+10, pos[1]+10)
    print("Celeiro")
    time.sleep(2)
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(2)
    moinhoMenu()

def moinhoMenu():
  pos = imagesearch("./img/icone_martelo.png")
  if pos[0] != -1:
    pyautogui.click(pos[0]+5, pos[1]+5)
    print("Abrindo menu")
    time.sleep(1)
    pos = imagesearch("./img/icone_celeiro_menu.png")
    if pos[0] != -1:
      if PRIMEIRA_ABERTURA_MENU_MOINHO:
        posicaoBotoesMoinho()
      global ITEM_CONTADOR_MOINHO
      pyautogui.click(POSICAO_BOTAO_MOINHO[ITEM_MOINHO[ITEM_CONTADOR_MOINHO]])
      if ITEM_CONTADOR_MOINHO == len(ITEM_MOINHO) -1:
        ITEM_CONTADOR_MOINHO = 0;
      else:
        ITEM_CONTADOR_MOINHO += 1
      return True
  return False

def cash():
  time.sleep(0.3)
  pos = imagesearch("./img/icone_cash.png")
  if pos[0] != -1:
    print("Cash")
    pyautogui.moveTo(pos[0]+5, pos[1]+5)


def zz():
  time.sleep(0.3)
  pos = imagesearch("./img/icone_zz.png")
  if pos[0] != -1:
    print("Encontrado ZZ")
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(1.5)
    if plantar() == False:
      if moinhoMenu() == False:
        if racaoGalinheiro() == False:
          if pomarMaca() == False:
            if racaoPorcos() == False:
              siloMenu()

def galinheiro():
  pos = imagesearch("./img/icone_ovos.png")
  if pos[0] != -1:
    print("Ovos")
    pyautogui.click(pos[0]+10, pos[1]+11)
    time.sleep(1.5)
    vender()
    pyautogui.click(pos[0]+10, pos[1]+11)
    time.sleep(1)
    racaoGalinheiro()

def racaoGalinheiro():
  pos = imagesearch("./img/icone_galinheiro_racao.png")
  if pos[0] != -1:
    print("Colocando ração")
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(0.5)
    return True
  return False

def colherPomar():
  pos = imagesearch("./img/icone_pomar_maca.png")
  if pos[0] != -1:
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(1.5)
    vender()
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(1)
    pomarMaca()

def pomarMaca():
  pos = imagesearch("./img/icone_plantacao_maca.png")
  if pos[0] != -1:
    pyautogui.click(pos[0]+5, pos[1]+10)
    return True
  return False

def buttonExit():
  pos = imagesearch("./img/botao_sair_vermelho.png")
  if pos[0] != -1:
    print("Fechando Menu")
    pyautogui.click(pos[0]+10, pos[1]+10)

def vender():
  posVenda = imagesearch("./img/botao_vender_verde.png")
  if posVenda[0] != -1:
    pyautogui.click(posVenda[0]+10, posVenda[1]+10)
    print("Vendendo")
    time.sleep(1.5)

def silo():
  pos = imagesearch("./img/icone_silo.png")
  if pos[0] != -1:
    pyautogui.click(pos[0]+10, pos[1]+10)
    print("Silo")
    time.sleep(1)
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(1)
    pyautogui.click(pos[0]+100, pos[1]+70)
    siloMenu()

def siloMenu():
  pos = imagesearch("./img/icone_adubo_menu.png")
  if pos[0] != -1:
    pyautogui.click(pos[0]-58, pos[1]+87)
    return True
  buttonExit()
  return False

def porcos():
  pos = imagesearch("./img/icone_porco.png")
  if pos[0] != -1:
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(1)
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(1)
    racaoPorcos()

def racaoPorcos():
  pos = imagesearch("./img/icone_racao_porcos_umida.png")
  if pos[0] != -1:
    pyautogui.click(pos[0]+10, pos[1]+10)
    time.sleep(0.5)
    pos = imagesearch("./img/icone_racao_porcos.png")
    if pos[0] != -1:
      pyautogui.click(pos[0]+10, pos[1]+10)
    return True
  return False

while True:
  cash()
  colherPlantar()
  moinho()
  galinheiro()
  colherPomar()
  silo()
  porcos()
  zz()
  buttonExit()
  time.sleep(2)
